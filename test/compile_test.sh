make -C ../ re
gcc -I ../includes/ -c test_simple.c
gcc -I ../includes/ -c test_number_conversion.c
gcc -I ../includes/ -c test_number_long_conversion.c
gcc -I ../includes/ -c test_number_length_modifier.c
gcc -I ../includes/ -c test_number_flags_simple.c
gcc -I ../includes/ -c test_number_flags_mixed.c
gcc -I ../includes/ -c test_number_flags_mixed_u.c
gcc -I ../includes/ -c test_base_flags_mixed.c
gcc -I ../includes/ -c test_octal_flags_mixed.c
gcc -I ../includes/ -c test_pointer_flags_mixed.c
gcc -I ../includes/ -c test_string.c
gcc -I ../includes/ -c test_bad.c
gcc -I ../includes/ -c test_wide_string.c
gcc -I ../includes/ -c test_wildcard.c
gcc -I ../includes/ -c test_dtoa.c
gcc -I ../includes/ -c test_float.c
gcc -I ../includes/ -c test_exponent.c
gcc -I ../includes/ -c test_gtype.c
gcc test_simple.o -L ../ -lftprintf -o simple
gcc test_number_conversion.o -L ../ -lftprintf -o number_conversion
gcc test_number_long_conversion.o -L ../ -lftprintf -o number_long_conversion
gcc test_number_length_modifier.o -L ../ -lftprintf -o number_length_modifier
gcc test_number_flags_simple.o -L ../ -lftprintf -o number_flags_simple
gcc test_number_flags_mixed.o -L ../ -lftprintf -o number_flags_mixed
gcc test_number_flags_mixed_u.o -L ../ -lftprintf -o number_flags_mixed_u
gcc test_base_flags_mixed.o -L ../ -lftprintf -o base_flags_mixed
gcc test_octal_flags_mixed.o -L ../ -lftprintf -o octal_flags_mixed
gcc test_pointer_flags_mixed.o -L ../ -lftprintf -o pointer_flags_mixed
gcc test_string.o -L ../ -lftprintf -o string
gcc test_wildcard.o -L ../ -lftprintf -o wildcard
gcc test_bad.o -L ../ -lftprintf -o bad
gcc test_wide_string.o -L ../ -lftprintf -o wide_string
gcc test_dtoa.o -L ../ -lftprintf -o dtoa
gcc test_float.o -L ../ -lftprintf -o float
gcc test_exponent.o -L ../ -lftprintf -o exponent
gcc test_gtype.o -L ../ -lftprintf -o gtype
rm test_*.o
