/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/15 15:35:48 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("(*d)%*d", 5, 42);
	printf("(%d)\n", ret);
	ret = printf("(*d)%*d", 5, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(*d)%*d", -5, 42);
	printf("(%d)\n", ret);
	ret = printf("(*d)%*d", -5, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(*d)%*d", 0, 42);
	printf("(%d)\n", ret);
	ret = printf("(*d)%*d", 0, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(*c)%*c", 0, 0);
	printf("(%d)\n", ret);
	ret = printf("(*c)%*c", 0, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(*c)%*c", 15, 0);
	printf("(%d)\n", ret);
	ret = printf("(*c)%*c", 15, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.*d)%.*d", 5, 42);
	printf("(%d)\n", ret);
	ret = printf("(.*d)%.*d", 5, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.*d)%.*d", -5, 42);
	printf("(%d)\n", ret);
	ret = printf("(.*d)%.*d", -5, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.*d)%.*d", 0, 42);
	printf("(%d)\n", ret);
	ret = printf("(.*d)%.*d", 0, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.*s)%.*s", 0, "42");
	printf("(%d)\n", ret);
	ret = printf("(.*s)%.*s", 0, "42");
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.*s)%.*s", 5, 0);
	printf("(%d)\n", ret);
	ret = printf("(.*s)%.*s", 5, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(3*p)%.*p", 10, 0);
	printf("(%d)\n", ret);
	ret = printf("(3*p)%.*p", 10, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(*.*d)%*.*d", 0, 3, 0);
	printf("(%d)\n", ret);
	ret = printf("(*.*d)%*.*d", 0, 3, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(3*d,0,0)%3*d", 0, 0);
	printf("(%d)\n", ret);
	ret = printf("(3*d,0,0)%3*d", 0, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(3*d,5,0)%3*d", 5, 0);
	printf("(%d)\n", ret);
	ret = printf("(3*d,5,0)%3*d", 5, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(05.*d,-15,42)%05.*d", -15, 42);
	printf("(%d)\n", ret);
	ret = printf("(05.*d,-15,42)%05.*d", -15, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
