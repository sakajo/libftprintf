/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 17:11:30 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0+5d)%0+5p||%0+5p||%0+5p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("(0+5p)%0+5p||%0+5p||%0+5p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0 -5p)%0 -5p||%0 -5p||%0 -5p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("(0 -5p)%0 -5p||%0 -5p||%0 -5p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0+5p)%0+5p||%0+5p||%0+5p", 1234567, 0, -1234567);
	printf("(%d)\n", ret);
	ret = printf("(0+5p)%0+5p||%0+5p||%0+5p", 1234567, 0, -1234567);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0+.5p)%0+.5p||%0+.5p||%0.5p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("(0+.5p)%0+.5p||%0+.5p||%0.5p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0+.0p)%0+.0p||%0+.0p||%0.0p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = ft_printf("(0+.0p)%0+.0p||%0+.0p||%0.0p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0 .0p)%0 .0p||%0 .0p||%0 .0p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("(0 .0p)%0 .0p||%0 .0p||%0 .0p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ft_putstr("---------------------------\n");
	ret = ft_printf("(+10.4p)%+10.4p||%10.4p||%10.4p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("(+10.4p)%+10.4p||%10.4p||%10.4p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(+8.4p)%+8.4p||%+8.4p||%+8.4p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("(+8.4p)%+8.4p||%+8.4p||%+8.4p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("( 8.0p)% 8.0p||% 8.0p||% 8.0p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("( 8.0p)% 8.0p||% 8.0p||% 8.0p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ft_putstr("---------------------------\n");
	ret = ft_printf("( 8.0p)%08.4p||%08.4p||%08.4p", 42, 0, -42);
	printf("(%d)\n", ret);
	ret = printf("( 8.0p)%08.4p||%08.4p||%08.4p", 42, 0, -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0-8.0p)%0-8.0p||%0-8.0p||%0-8.0p", 123456, 0, -123456);
	printf("(%d)\n", ret);
	ret = printf("(0-8.0p)%0-8.0p||%0-8.0p||%0-8.0p", 123456, 0, -123456);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(08.4p)%08.4p||%08.4p||%08.4p", 56, 0, -56);
	printf("(%d)\n", ret);
	ret = printf("(08.4p)%08.4p||%08.4p||%08.4p", 56, 0, -56);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ft_putstr("---------------------------\n");
	ret = ft_printf("(-+5p)%-+5p||%-+5p||%-+5p", 123456, 0, -123456);
	printf("(%d)\n", ret);
	ret = printf("(-+5p)%-+5p||%-+5p||%-+5p", 123456, 0, -123456);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(#.3p)%#.3p||%#.3p||%#.3p", 56, 0, -56);
	printf("(%d)\n", ret);
	ret = printf("(#.3p)%#.3p||%#.3p||%#.3p", 56, 0, -56);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(#.0p)%#.0p||%#.0p||%#.0p", 56, 0, -56);
	printf("(%d)\n", ret);
	ret = printf("(#.0p)%#.0p||%#.0p||%#.0p", 56, 0, -56);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
