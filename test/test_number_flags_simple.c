/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 19:30:57 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("(10d):%10d", 42);
	printf("(%d)\n", ret);
	ret = printf("(10d):%10d", 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(10d):%10d", -42);
	printf("(%d)\n", ret);
	ret = printf("(10d):%10d", -42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(10d):%10d", 0);
	printf("(%d)\n", ret);
	ret = printf("(10d):%10d", 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(-10d):%-10d", 42);
	printf("(%d)\n", ret);
	ret = printf("(-10d):%-10d", 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.10d):%.10d", 42);
	printf("(%d)\n", ret);
	ret = printf("(.10d):%.10d", 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.10d):%.10d %.10d", -42, 0);
	printf("(%d)\n", ret);
	ret = printf("(.10d):%.10d %.10d", -42, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.0d):%.0d %.0d %.0d", -42, 0, 42);
	printf("(%d)\n", ret);
	ret = printf("(.0d):%.0d %.0d %.0d", -42, 0, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0d):%0d %0d %0d", -42, 0, 42);
	printf("(%d)\n", ret);
	ret = printf("(0d):%0d %0d %0d", -42, 0, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("( d):% d % d % d", -42, 0, 42);
	printf("(%d)\n", ret);
	ret = printf("( d):% d % d % d", -42, 0, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(+d):%+d %+d %+d", -42, 0, 42);
	printf("(%d)\n", ret);
	ret = printf("(+d):%+d %+d %+d", -42, 0, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(#d):%#d %#d %#d", -42, 0, 42);
	printf("(%d)\n", ret);
	ret = printf("(#d):%#d %#d %#d", -42, 0, 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
