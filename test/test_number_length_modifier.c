/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/10 21:48:25 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("%ld%ld", 0, 42);
	printf("(%d)\n", ret);
	ret = printf("%ld%ld", (long)0, (long)42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%ld", (long)INT_MAX + 1);
	printf("(%d)\n", ret);
	ret = printf("%ld", (long)INT_MAX + 1);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%ld", (long)INT_MIN - 1);
	printf("(%d)\n", ret);
	ret = printf("%ld", (long)INT_MIN - 1);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%ld", LONG_MAX);
	printf("(%d)\n", ret);
	ret = printf("%ld", LONG_MAX);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%ld", LONG_MIN);
	printf("(%d)\n", ret);
	ret = printf("%ld", LONG_MIN);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%ld", LONG_MAX);
	printf("(%d)\n", ret);
	ret = printf("%ld", LONG_MAX);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%li%li", 0, 42);
	printf("(%d)\n", ret);
	ret = printf("%li%li", (long)0, (long)42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%li", (long)INT_MAX + 1);
	printf("(%d)\n", ret);
	ret = printf("%li", (long)INT_MAX + 1);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%li", (long)INT_MIN - 1);
	printf("(%d)\n", ret);
	ret = printf("%li", (long)INT_MIN - 1);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%li", LONG_MAX);
	printf("(%d)\n", ret);
	ret = printf("%li", LONG_MAX);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%li", LONG_MIN);
	printf("(%d)\n", ret);
	ret = printf("%li", LONG_MIN);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%lu", ULONG_MAX);
	printf("(%d)\n", ret);
	ret = printf("%lu", ULONG_MAX);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%lld%lld", (long long)0, (long long)42);
	printf("(%d)\n", ret);
	ret = printf("%lld%lld", (long long)0, (long long)42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%lli%lli", (long long)0, (long long)42);
	printf("(%d)\n", ret);
	ret = printf("%lli%lli", (long long)0, (long long)42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
