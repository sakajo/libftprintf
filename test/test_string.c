/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_string.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/15 23:38:12 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:40:20 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("(10s)%10s||%10c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(10s)%10s||%10c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(010s)%010s||%010c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(010s)%010s||%010c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(3s)%3s||%3c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(3s)%3s||%3c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.10s)%.10s||%.10c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(.10s)%.10s||%.10c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.0s)%.0s||%.0c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(.0s)%.0s||%.0c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.3s)%.3s||%.3c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(.3s)%.3s||%.3c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(-10s)%-10s||%-10c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(-10s)%-10s||%-10c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(0-10s)%0-10s||%0-10c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(0-10s)%0-10s||%0-10c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(010s)%010s||%010c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(010s)%010s||%010c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(010.3s)%010.3s||%010.3c", "string", 'c');
	printf("(%d)\n", ret);
	ret = ft_printf("(010.3s)%010.3s||%010.3c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(010.8s)%010.8s||%010.8c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(010.8s)%010.8s||%010.8c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(010.s)%010.s||%010.c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(010.s)%010.s||%010.c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(05.0s)%05.0s||%05.0c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(05.0s)%05.0s||%05.0c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(5.0s)%5.0s||%5.0c", "string", 'c');
	printf("(%d)\n", ret);
	ret = printf("(5.0s)%5.0s||%5.0c", "string", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
