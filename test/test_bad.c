/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 01:53:14 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("(B)%B||%%||%");
	printf("(%d)\n", ret);
	ret = printf("(B)%B||%%||%");
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(5B)%5B||%5%||%5");
	printf("(%d)\n", ret);
	ret = printf("(5B)%5B||%5%||%5");
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(05B)%05B||%05%||%05c||%05", 'c');
	printf("(%d)\n", ret);
	ret = printf("(05B)%05B||%05%||%05c||%05", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(-5B)%-5B||%-5%||%-05");
	printf("(%d)\n", ret);
	ret = printf("(-5B)%-5B||%-5%||%-05");
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.5B)%.5B||%.5%||%.5");
	printf("(%d)\n", ret);
	ret = printf("(.5B)%.5B||%.5%||%.5");
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(.0B)%.0B||%.0%||%.0");
	printf("(%d)\n", ret);
	ret = printf("(.0B)%.0B||%.0%||%.0");
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(5.0B)%5.0B||%5.0%||%5.0c||%5.0", 'c');
	printf("(%d)\n", ret);
	ret = printf("(5.0B)%5.0B||%5.0%||%5.0c||%5.0", 'c');
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("(05.B)%05.B||%05.%||%05.");
	printf("(%d)\n", ret);
	ret = printf("(05.B)%05.B||%05.%||%05.");
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
