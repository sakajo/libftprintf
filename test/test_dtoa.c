/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/15 15:35:48 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	int		ret;
	
	ft_putstr(ft_dtoa(1.42, 2));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(1.42, 3));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(1.42, 5));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(1.42, 8));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(1.42, 12));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(1.42, 18));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(-1.42, 6));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(1444565444646.6465424242242, 6));
	ft_putchar('\n');
	ft_putstr(ft_dtoa(-1444565444646.6465424242242, 6));
	ft_putchar('\n');
	printf("%f\n", 1.42);
	printf("%f\n", -1.42);
	printf("%f\n", 1444565444646.646542424242242);
	printf("%f\n", -1444565444646.646542424242242);
	return (0);
}
