/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/19 00:26:48 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("%a\n", 1.42);
	printf("(%d)\n", ret);
	ret = printf("%a\n", 1.42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%g\n", 1.42);
	printf("(%d)\n", ret);
	ret = printf("%g\n", 1.42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%g\n", 34567.42);
	printf("(%d)\n", ret);
	ret = printf("%g\n", 34567.42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%g\n", 1234567.42);
	printf("(%d)\n", ret);
	ret = printf("%g\n", 1234567.42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%g\n", 0.0000000042123456789);
	printf("(%d)\n", ret);
	ret = printf("%g\n", 0.0000000042123456789);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%e\n", 1.42);
	printf("(%d)\n", ret);
	ret = printf("%e\n", 1.42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%e\n", 4231.42);
	printf("(%d)\n", ret);
	ret = printf("%e\n", 4231.42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%e\n", 0.042);
	printf("(%d)\n", ret);
	ret = printf("%e\n", 0.042);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
