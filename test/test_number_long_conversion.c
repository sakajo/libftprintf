/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/10 21:48:25 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("%D", 0L);
	printf("(%d)\n", ret);
	ret = printf("%ld", 0L);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%D, %D", -1L, 1L);
	printf("(%d)\n", ret);
	ret = printf("%ld, %ld", -1L, 1L);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("max long: %D", LONG_MAX);
	printf("(%d)\n", ret);
	ret = printf("max long: %ld", LONG_MAX);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%D<- min long", LONG_MIN);
	printf("(%d)\n", ret);
	ret = printf("%ld<- min long", LONG_MIN);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
