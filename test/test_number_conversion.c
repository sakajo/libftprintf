/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_printf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:53:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/10 21:48:25 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include <limits.h>

int		main(void)
{
	void	*ptr;
	int		ret;
	
	ft_putstr("---------------------------\n");
	ret = ft_printf("%d", 42);
	printf("(%d)\n", ret);
	ret = printf("%d", 42);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%d, %d", -42, 0);
	printf("(%d)\n", ret);
	ret = printf("%d, %d", -42, 0);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("max int: %d", INT_MAX);
	printf("(%d)\n", ret);
	ret = printf("max int: %d", INT_MAX);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	ret = ft_printf("%d<- min int", INT_MIN);
	printf("(%d)\n", ret);
	ret = printf("%d<- min int", INT_MIN);
	printf("(%d)\n", ret);
	ft_putstr("---------------------------\n");
	return (0);
}
