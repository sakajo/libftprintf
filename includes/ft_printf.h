/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:36:42 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/19 00:50:09 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <stdarg.h>

# define FLAGS " .#0123456789-+hljz*L"

typedef struct	s_info
{
	char		*flags;
	char		conversion;
	int			precision;
	int			width;
	char		sign;
	char		zero;
	char		minus;
	char		sharp;
}				t_info;

int				ft_printf(const char *format, ...);
void			print_format(const char *format, va_list ap, int *bytes);
void			print_argument(va_list ap, t_info *info, int *bytes);

t_info			*get_info(va_list ap, const char *format);
int				get_width(char *argument);
int				get_precision(char *argument);
int				flag_exists(const char *try, char *argument);

void			decimal_print_argument(va_list ap,
						t_info *info, int *bytes);
char			*decimal_get_string(va_list ap, t_info *info);
char			*decimal_split_sign(char **number, t_info *info);
void			base_print_argument(va_list ap,
						t_info *info, int *bytes);
char			*base_get_string(va_list ap, t_info *info);
char			*base_get_sign(char *string, t_info *info);
void			number_edit_precision(char **number,
						int precision, char conversion);
void			number_edit_width(char **number, t_info *info, char *sign);

void			string_print_argument(va_list ap, t_info *info, int *bytes);
char			*string_get_string(va_list ap,
						char conversion, int *zero_character);
void			string_edit_precision(char **string, int precision, char conversion);
void			string_edit_width(char **string,
						t_info *info, int zero_character);
void			string_print(char *string, int zero_character, int *bytes);

void			wide_print_argument(va_list ap, t_info *info, int *bytes);
void			wide_string_edit_precision(wchar_t **wide_string,
						int precision, char conversion);
void			wide_string_edit_width(wchar_t **wide_string,
						t_info *info, int zero_character);
wchar_t			*wide_get_wide_string(va_list ap,
						char conversion, int *zero_character);
void			wide_string_print(wchar_t *wide_string,
						int zero_boolean, int *bytes);

void			float_print_argument(va_list ap, t_info *info, int *bytes);

void			exponent_print_argument(va_list ap, t_info *info, int *bytes);
long double		exponent_adjust_number(long double number, int *exponent);
char			*exponent_get_tail(int exponent);

void			bad_print_argument(t_info *info, int *bytes);

#endif
