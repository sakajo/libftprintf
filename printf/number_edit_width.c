/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_edit_width.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:01:34 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/19 00:48:06 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	number_add_zero_width(char **number, int width, int length)
{
	char	*new_number;
	char	*fill;

	if (width <= length)
		return ;
	fill = ft_strnew(width - length);
	ft_memset(fill, '0', width - length);
	new_number = ft_strjoin(fill, *number);
	ft_strdel(&fill);
	ft_strdel(number);
	*number = new_number;
}

static void	number_add_normal_width(char **number, t_info *info, int length)
{
	char	*new_number;
	char	*fill;

	if (info->width > length)
	{
		fill = ft_strnew(info->width - length);
		ft_memset(fill, ' ', info->width - length);
		if (info->minus)
			new_number = ft_strjoin(*number, fill);
		else
			new_number = ft_strjoin(fill, *number);
		ft_strdel(number);
		ft_strdel(&fill);
		*number = new_number;
	}
}

static void	number_join_sign(char **number, char *sign)
{
	char	*new_number;

	if (ft_strequ("0", sign) && (*number)[0] == '0')
		return ;
	new_number = ft_strjoin(sign, *number);
	ft_strdel(number);
	*number = new_number;
}

void		number_edit_width(char **number, t_info *info, char *sign)
{
	int		length;

	length = ft_strlen(*number) + ft_strlen(sign);
	if ((info->precision == -1 || ft_isanyof(info->conversion, "fFeEgGaA")) &&
			info->minus == 0 && info->zero)
		number_add_zero_width(number, info->width, length);
	number_join_sign(number, sign);
	length = ft_strlen(*number);
	number_add_normal_width(number, info, length);
}
