/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bad_print_argument.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:34:34 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 19:08:26 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	bad_print_argument(t_info *info, int *bytes)
{
	char	*string;

	if (info->conversion == '\0')
		return ;
	string = ft_strnew(1);
	string[0] = info->conversion;
	info->conversion = 'c';
	string_edit_precision(&string,
			info->precision, info->conversion);
	string_edit_width(&string, info, 0);
	ft_putstr(string);
	(*bytes) += ft_strlen(string);
}
