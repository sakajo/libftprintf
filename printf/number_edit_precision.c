/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_edit_precision.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:00:49 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 20:37:27 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	number_edit_precision(char **number, int precision, char conversion)
{
	char	*new_number;
	int		length;

	if (precision == 0 && ft_strequ(*number, "0") &&
			!ft_isanyof(conversion, "fF"))
	{
		ft_strdel(number);
		*number = ft_strnew(0);
		return ;
	}
	length = ft_strlen(*number);
	if (precision > length)
	{
		new_number = ft_strnew(precision);
		ft_memset(new_number, '0', precision);
		ft_strcpy(new_number + precision - length, *number);
		ft_strdel(number);
		*number = new_number;
	}
}
