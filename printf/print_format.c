/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_format.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:07:42 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 21:08:08 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	print_format(const char *format, va_list ap, int *bytes)
{
	t_info		*info;
	int			index;

	index = 0;
	while (format[index] != '\0')
	{
		if (format[index] == '%')
		{
			index++;
			info = get_info(ap, format + index);
			print_argument(ap, info, bytes);
			while (ft_isanyof(format[index], FLAGS))
				index++;
			if (format[index] != '\0')
				index++;
		}
		else
		{
			ft_putchar(format[index]);
			(*bytes)++;
			index++;
		}
	}
}
