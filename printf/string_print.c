/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_print.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:28:22 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:28:28 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	string_print(char *string, int zero_character, int *bytes)
{
	int		index;

	index = 0;
	while (string[index] != '\0')
	{
		ft_putchar(string[index]);
		(*bytes)++;
		index++;
	}
	if (zero_character)
	{
		ft_putchar(string[index]);
		(*bytes)++;
		index++;
		while (string[index] != '\0')
		{
			ft_putchar(string[index]);
			(*bytes)++;
			index++;
		}
	}
	zero_character += 0;
}
