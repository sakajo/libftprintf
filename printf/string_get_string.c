/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_get_string.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:27:43 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:37:41 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*string_get_string(va_list ap, char conversion, int *zero_character)
{
	char	*string;

	if (conversion == 's')
	{
		string = ft_strdup(va_arg(ap, char*));
		if (!string)
			string = ft_strdup("(null)");
	}
	else
	{
		string = ft_strnew(1);
		string[0] = va_arg(ap, int);
		if (string[0] == '\0')
			*zero_character = 1;
	}
	return (string);
}
