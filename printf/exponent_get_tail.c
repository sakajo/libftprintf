/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exponent_get_tail.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 23:23:50 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/19 00:53:41 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*exponent_get_tail(int exponent)
{
	char	*exponent_string;

	exponent_string = ft_strnew(4);
	exponent_string[0] = 'e';
	if (exponent >= 0)
		exponent_string[1] = '+';
	else
		exponent_string[1] = '-';
	if (exponent < 0)
		exponent *= (-1);
	exponent_string[2] = exponent / 10 + '0';
	exponent_string[3] = exponent % 10 + '0';
	return (exponent_string);
}
