/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_print_argument.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:11:10 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 19:04:55 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	string_print_argument(va_list ap, t_info *info, int *bytes)
{
	char	*string;
	int		zero_character;

	zero_character = 0;
	string = string_get_string(ap, info->conversion, &zero_character);
	string_edit_precision(&string,
			info->precision, info->conversion);
	string_edit_width(&string, info, zero_character);
	string_print(string, zero_character, bytes);
}
