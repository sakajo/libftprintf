/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base_print_argument.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:38:13 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 20:54:11 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	base_print_argument(va_list ap, t_info *info, int *bytes)
{
	char	*string;
	char	*sign;

	string = base_get_string(ap, info);
	sign = base_get_sign(string, info);
	number_edit_precision(&string, info->precision, info->conversion);
	number_edit_width(&string, info, sign);
	if (info->conversion == 'X')
		ft_strupcase(string);
	ft_putstr(string);
	(*bytes) += ft_strlen(string);
}
