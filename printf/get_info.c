/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_info.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:58:27 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 22:30:40 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	initialize_info(t_info *info)
{
	info->precision = -1;
	info->width = 0;
	info->sign = 0;
	info->zero = 0;
	info->minus = 0;
	info->sharp = 0;
}

void	get_info_precision(t_info *info,
				va_list ap, const char *format, int *index)
{
	(*index)++;
	if (format[*index] == '*')
	{
		info->precision = va_arg(ap, int);
		if (info->precision < 0)
			info->precision = (-1);
	}
	else
	{
		if (!ft_isdigit(format[*index]))
			info->precision = 0;
		else
		{
			info->precision = ft_atoi(format + (*index));
			(*index) += ft_numlen(info->precision) - 1;
		}
	}
}

void	get_wildcard_width(t_info *info, va_list ap)
{
	info->width = va_arg(ap, int);
	if (info->width < 0)
	{
		info->width = info->width * (-1);
		info->minus = 1;
	}
}

void	choose_info(t_info *info, va_list ap, const char *format, int *index)
{
	if (format[*index] == '+' || format[*index] == ' ')
	{
		if (info->sign != '+')
			info->sign = format[*index];
	}
	else if (format[*index] == '0')
		info->zero = 1;
	else if (format[*index] == '-')
		info->minus = 1;
	else if (format[*index] == '#')
		info->sharp = 1;
	else if (format[*index] == '*')
		get_wildcard_width(info, ap);
	else if (format[*index] == '.')
		get_info_precision(info, ap, format, index);
	else if (ft_isdigit(format[*index]))
	{
		info->width = ft_atoi(format + (*index));
		(*index) += ft_numlen(info->width) - 1;
	}
}

t_info	*get_info(va_list ap, const char *format)
{
	t_info	*info;
	int		index;

	info = (t_info*)malloc(sizeof(t_info));
	initialize_info(info);
	index = 0;
	while (ft_isanyof(format[index], FLAGS))
	{
		choose_info(info, ap, format, &index);
		index++;
	}
	index = 0;
	while (ft_isanyof(format[index], FLAGS))
		index++;
	info->flags = ft_strnew(index);
	ft_strncpy(info->flags, format, index);
	info->conversion = format[index];
	return (info);
}
