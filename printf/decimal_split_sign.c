/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decimal_split_sign.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:55:06 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 18:40:21 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*decimal_split_sign(char **number, t_info *info)
{
	char	*sign;
	char	*new_number;

	if (info->conversion == 'u' || info->conversion == 'U')
		return (ft_strnew(0));
	if (*number[0] == '-')
	{
		sign = ft_strdup("-");
		new_number = ft_strdup(*number + 1);
		ft_strdel(number);
		*number = new_number;
	}
	else if (info->sign != 0)
	{
		sign = ft_strnew(1);
		sign[0] = info->sign;
	}
	else
		sign = ft_strnew(0);
	return (sign);
}
