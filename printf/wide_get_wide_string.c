/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wide_get_wide_string.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:19:33 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:19:45 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

wchar_t	*wide_get_wide_string(va_list ap, char conversion, int *zero_character)
{
	wchar_t	*wide_string;

	if (conversion == 's' || conversion == 'S')
	{
		wide_string = ft_wide_string_dup(va_arg(ap, wchar_t*));
		if (!wide_string)
		{
			wide_string = ft_wide_string_new(6);
			wide_string[0] = '(';
			wide_string[1] = 'n';
			wide_string[2] = 'u';
			wide_string[3] = 'l';
			wide_string[4] = 'l';
			wide_string[5] = ')';
		}
	}
	else
	{
		wide_string = ft_wide_string_new(1);
		wide_string[0] = va_arg(ap, wchar_t);
		if (wide_string[0] == 0)
			*zero_character = 1;
	}
	return (wide_string);
}
