/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_edit_precision.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:08:26 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 07:05:27 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	string_edit_precision(char **string, int precision, char conversion)
{
	char	*new_string;
	int		length;

	if (precision < 0 || (precision == 0 && conversion == 'c'))
		return ;
	length = ft_strlen(*string);
	if (precision < length)
	{
		new_string = ft_strsub(*string, 0, precision);
		ft_strdel(string);
		*string = new_string;
	}
}
