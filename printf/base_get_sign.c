/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base_get_sign.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:42:04 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 20:50:52 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*base_get_sign(char *string, t_info *info)
{
	if (info->conversion == 'p')
		return (ft_strdup("0x"));
	if (!info->sharp)
		return (ft_strnew(0));
	if (info->conversion == 'x' || info->conversion == 'X')
	{
		if (!ft_strequ("0", string) && !ft_strequ("", string))
			return (ft_strdup("0x"));
	}
	else
	{
		return (ft_strdup("0"));
	}
	return (ft_strnew(0));
}
