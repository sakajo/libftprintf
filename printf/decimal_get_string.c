/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decimal_get_string.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:50:22 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 07:13:01 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*decimal_unsigned_get_string(va_list ap, t_info *info)
{
	char	*number;

	if (flag_exists("z", info->flags))
		number = ft_ulltoa(va_arg(ap, size_t));
//	else if (flag_exists("j", info->flags))
//		number = ft_ulltoa(va_arg(ap, uintmax_t));
	else if (flag_exists("ll", info->flags))
		number = ft_ulltoa(va_arg(ap, unsigned long long));
	else if (flag_exists("l", info->flags) || info->conversion == 'U')
		number = ft_ulltoa(va_arg(ap, unsigned long));
	else if (!flag_exists("h", info->flags))
		number = ft_uitoa(va_arg(ap, unsigned int));
	else if (!flag_exists("hh", info->flags))
		number = ft_uitoa((unsigned short)va_arg(ap, unsigned int));
	else
		number = ft_uitoa((unsigned char)va_arg(ap, unsigned int));
	return (number);
}

static char	*decimal_signed_get_string(va_list ap, t_info *info)
{
	char	*number;

	if (flag_exists("z", info->flags))
		number = ft_lltoa(va_arg(ap, ssize_t));
//	else if (flag_exists("j", info->flags))
//		number = ft_lltoa(va_arg(ap, intmax_t));
	else if (flag_exists("ll", info->flags))
		number = ft_lltoa(va_arg(ap, long long));
	else if (flag_exists("l", info->flags) || info->conversion == 'D')
		number = ft_ltoa(va_arg(ap, long));
	else if (!flag_exists("h", info->flags))
		number = ft_itoa(va_arg(ap, int));
	else if (!flag_exists("hh", info->flags))
		number = ft_itoa((short)va_arg(ap, int));
	else
		number = ft_itoa((char)va_arg(ap, int));
	return (number);
}

char		*decimal_get_string(va_list ap, t_info *info)
{
	if (info->conversion == 'u' || info->conversion == 'U')
		return (decimal_unsigned_get_string(ap, info));
	else
		return (decimal_signed_get_string(ap, info));
}
