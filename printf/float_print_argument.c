/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   float_print_argument.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 22:23:18 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/19 00:51:15 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	float_print_argument(va_list ap, t_info *info, int *bytes)
{
	char	*string;
	char	*sign;

	if (info->precision == -1)
		info->precision = 6;
	if (ft_isanyof('L', info->flags))
		string = ft_dtoa(va_arg(ap, long double), info->precision);
	else
		string = ft_dtoa(va_arg(ap, double), info->precision);
	sign = decimal_split_sign(&string, info);
	number_edit_precision(&string, info->precision, info->conversion);
	number_edit_width(&string, info, sign);
	ft_putstr(string);
	(*bytes) += ft_strlen(string);
}
