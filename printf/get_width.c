/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_width.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:00:39 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 07:26:35 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*jump_through_flags(char *argument)
{
	int index;

	index = 0;
	while (!(argument[index] >= '1' && argument[index] <= '9')
			&& argument[index] != '\0' && argument[index] != '.'
			&& argument[index] != '*')
		index++;
	if (argument[index] == '.' || argument[index] == '*')
	{
		index++;
		while (argument[index] >= '0' && argument[index] <= '9')
			index++;
		return (jump_through_flags(argument + index));
	}
	return (argument + index);
}

static int	get_width_value(char *argument, int minus_boolean)
{
	int		width;
	int		sign;
	int		index;

	width = 0;
	sign = 1;
	index = 0;
	if (minus_boolean)
		sign = -1;
	while (argument[index] >= '0' && argument[index] <= '9')
	{
		width = width * 10 + argument[index] - '0';
		index++;
	}
	return (width * sign);
}

int			get_width(char *argument)
{
	int		minus_boolean;

	minus_boolean = flag_exists("-", argument);
	argument = jump_through_flags(argument);
	return (get_width_value(argument, minus_boolean));
}
