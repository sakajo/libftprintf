/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_edit_width.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:08:54 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 20:29:30 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	string_width_join(char **string,
				char **fill, int width, int negative_boolean)
{
	char	*new_string;

	new_string = ft_strnew(width);
	if (negative_boolean)
	{
		ft_strcpy(new_string, *string);
		ft_strcpy(new_string + width - ft_strlen(*fill), *fill);
	}
	else
	{
		ft_strcpy(new_string, *fill);
		ft_strcpy(new_string + ft_strlen(*fill), *string);
	}
	ft_strdel(string);
	ft_strdel(fill);
	*string = new_string;
}

void		string_edit_width(char **string,
				t_info *info, int zero_character)
{
	char	*fill;
	int		length;

	length = ft_strlen(*string) + zero_character;
	if (info->width > length)
	{
		fill = ft_strnew(info->width - length);
		if (info->zero && !info->minus)
			ft_memset(fill, '0', info->width - length);
		else
			ft_memset(fill, ' ', info->width - length);
		string_width_join(string, &fill, info->width, info->minus);
	}
}
