/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wide_string_edit_width.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:22:24 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 19:27:25 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	wide_string_set(wchar_t *wide_string, wchar_t character, int size)
{
	int		index;

	index = 0;
	while (index < size)
	{
		wide_string[index] = character;
		index++;
	}
}

static void	wide_string_copy(wchar_t *dest, wchar_t *source)
{
	int		index;

	index = 0;
	while (source[index] != '\0')
	{
		dest[index] = source[index];
		index++;
	}
}

static void	wide_string_width_join(wchar_t **wide_string,
				wchar_t **fill, int zero_character, int negative_boolean)
{
	wchar_t	*new_string;

	new_string = ft_wide_string_new(ft_wide_string_length(*fill) +
			ft_wide_string_length(*wide_string) + zero_character);
	if (negative_boolean)
	{
		wide_string_copy(new_string, *wide_string);
		wide_string_copy(new_string + zero_character +
				ft_wide_string_length(*wide_string), *fill);
	}
	else
	{
		wide_string_copy(new_string, *fill);
		wide_string_copy(new_string + ft_wide_string_length(*fill),
				*wide_string);
	}
	*wide_string = new_string;
}

void		wide_string_edit_width(wchar_t **wide_string,
				t_info *info, int zero_character)
{
	wchar_t	*fill;
	int		size;

	size = ft_wide_string_size(*wide_string) + zero_character;
	if (info->width > size)
	{
		fill = ft_wide_string_new(info->width - size);
		if (!info->minus && info->zero)
			wide_string_set(fill, '0', info->width - size);
		else
			wide_string_set(fill, ' ', info->width - size);
		wide_string_width_join(wide_string,
				&fill, zero_character, info->minus);
	}
}
