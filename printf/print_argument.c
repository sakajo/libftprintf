/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_argument.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:02:31 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/19 00:49:15 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	print_argument(va_list ap, t_info *info, int *bytes)
{
	if (ft_isanyof(info->conversion, "diuDU"))
		decimal_print_argument(ap, info, bytes);
	else if (ft_isanyof(info->conversion, "xXoOpb"))
		base_print_argument(ap, info, bytes);
	else if (ft_isanyof(info->conversion, "sc") &&
			!flag_exists("l", info->flags))
		string_print_argument(ap, info, bytes);
	else if (ft_isanyof(info->conversion, "sScC"))
		wide_print_argument(ap, info, bytes);
	else if (ft_isanyof(info->conversion, "fF"))
		float_print_argument(ap, info, bytes);
	else if (ft_isanyof(info->conversion, "eE"))
		exponent_print_argument(ap, info, bytes);
	else
		bad_print_argument(info, bytes);
}
