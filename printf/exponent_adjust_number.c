/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exponent_adjust_number.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 23:24:11 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 23:41:27 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

long double	exponent_adjust_number(long double number, int *exponent)
{
	int		negative_boolean;

	(*exponent) = 0;
	negative_boolean = 0;
	if (number < 0)
	{
		negative_boolean = 1;
		number *= (-1);
	}
	if (number == 0)
		return (number);
	while (number >= 10)
	{
		number /= 10;
		(*exponent)++;
	}
	while (number < 1)
	{
		number *= 10;
		(*exponent)--;
	}
	if (negative_boolean)
		return (number * (-1));
	else
		return (number);
}
