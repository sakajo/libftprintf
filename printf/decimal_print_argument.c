/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decimal_print_argument.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:45:22 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 20:54:04 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	decimal_print_argument(va_list ap, t_info *info, int *bytes)
{
	char	*number;
	char	*sign;

	number = decimal_get_string(ap, info);
	sign = decimal_split_sign(&number, info);
	number_edit_precision(&number, info->precision, info->conversion);
	number_edit_width(&number, info, sign);
	ft_putstr(number);
	(*bytes) += ft_strlen(number);
}
