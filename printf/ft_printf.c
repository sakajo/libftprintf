/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:57:36 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:23:39 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_printf(const char *format, ...)
{
	int		bytes;
	va_list	ap;

	bytes = 0;
	va_start(ap, format);
	print_format(format, ap, &bytes);
	va_end(ap);
	return (bytes);
}
