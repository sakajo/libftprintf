/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_precision.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:59:05 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 20:59:33 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				get_precision(char *argument)
{
	int		precision;
	int		index;

	index = 0;
	while (argument[index] != '\0' && argument[index] != '.')
		index++;
	if (argument[index] == '\0')
		return (-1);
	index++;
	precision = 0;
	while (ft_isdigit(argument[index]))
	{
		precision *= 10;
		precision += argument[index] - '0';
		index++;
	}
	return (precision);
}
