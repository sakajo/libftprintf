/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wide_string_edit_precision.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:22:07 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:22:09 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

wchar_t	*wide_string_sub(wchar_t *source, int size)
{
	wchar_t	*sub;
	int		index;
	int		length;

	length = 0;
	index = 0;
	while (index + (int)ft_wide_char_size(source[length]) <= size)
	{
		index += ft_wide_char_size(source[length]);
		length++;
	}
	sub = ft_wide_string_new(length);
	index = 0;
	while (index < length)
	{
		sub[index] = source[index];
		index++;
	}
	return (sub);
}

void	wide_string_edit_precision(wchar_t **wide_string,
				int precision, char conversion)
{
	wchar_t	*new_string;
	int		size;

	if (precision == -1 || (precision == 0 &&
				(conversion == 'c' || conversion == 'C')))
		return ;
	size = ft_wide_string_size(*wide_string);
	if (precision < size)
	{
		new_string = wide_string_sub(*wide_string, precision);
		free(*wide_string);
		*wide_string = new_string;
	}
}
