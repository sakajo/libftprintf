/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exponent_print_argument.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 22:34:20 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/19 00:52:57 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	exponent_print_argument(va_list ap, t_info *info, int *bytes)
{
	char		*string;
	long double	number;
	int			exponent;
	char		*sign;

	if (info->precision == -1)
		info->precision = 6;
	if (ft_isanyof('L', info->flags))
		number = va_arg(ap, long double);
	else
		number = va_arg(ap, double);
	number = exponent_adjust_number(number, &exponent);
	string = ft_strjoin(ft_dtoa(number,
				info->precision), exponent_get_tail(exponent));
	sign = decimal_split_sign(&string, info);
	number_edit_precision(&string, info->precision, info->conversion);
	number_edit_width(&string, info, sign);
	ft_putstr(string);
	(*bytes) += ft_strlen(string);
}
