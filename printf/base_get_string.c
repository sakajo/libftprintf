/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base_get_string.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:51:34 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 20:21:37 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*binary_get_string(va_list ap, t_info *info)
{
	char	*binary;

	if (flag_exists("z", info->flags))
		binary = ft_utobin(va_arg(ap, size_t));
//	else if (flag_exists("j", info->flags))
//		binary = ft_utobin(va_arg(ap, uintmax_t));
	else if (flag_exists("ll", info->flags))
		binary = ft_utobin(va_arg(ap, unsigned long long));
	else if (flag_exists("l", info->flags))
		binary = ft_utobin(va_arg(ap, unsigned long));
	else if (!flag_exists("h", info->flags))
		binary = ft_utobin(va_arg(ap, unsigned int));
	else if (!flag_exists("hh", info->flags))
		binary = ft_utobin((unsigned short)va_arg(ap, unsigned int));
	else
		binary = ft_utobin((unsigned char)va_arg(ap, unsigned int));
	return (binary);
}

static char	*octal_get_string(va_list ap, t_info *info)
{
	char	*octal;

	if (flag_exists("z", info->flags))
		octal = ft_utooctal(va_arg(ap, size_t));
//	else if (flag_exists("j", info->flags))
//		octal = ft_utooctal(va_arg(ap, uintmax_t));
	else if (flag_exists("ll", info->flags))
		octal = ft_utooctal(va_arg(ap, unsigned long long));
	else if (flag_exists("l", info->flags) || info->conversion == 'O')
		octal = ft_utooctal(va_arg(ap, unsigned long));
	else if (!flag_exists("h", info->flags))
		octal = ft_utooctal(va_arg(ap, unsigned int));
	else if (!flag_exists("hh", info->flags))
		octal = ft_utooctal((unsigned short)va_arg(ap, unsigned int));
	else
		octal = ft_utooctal((unsigned char)va_arg(ap, unsigned int));
	return (octal);
}

static char	*hexa_get_string(va_list ap, t_info *info)
{
	char	*hexa;

	if (flag_exists("z", info->flags))
		hexa = ft_utohexa(va_arg(ap, size_t));
//	else if (flag_exists("j", info->flags))
//		hexa = ft_utohexa(va_arg(ap, uintmax_t));
	else if (flag_exists("ll", info->flags))
		hexa = ft_utohexa(va_arg(ap, unsigned long long));
	else if (flag_exists("l", info->flags) || info->conversion == 'p')
		hexa = ft_utohexa(va_arg(ap, unsigned long));
	else if (!flag_exists("h", info->flags))
		hexa = ft_utohexa(va_arg(ap, unsigned int));
	else if (!flag_exists("hh", info->flags))
		hexa = ft_utohexa((unsigned short)va_arg(ap, unsigned int));
	else
		hexa = ft_utohexa((unsigned char)va_arg(ap, unsigned int));
	return (hexa);
}

char		*base_get_string(va_list ap, t_info *info)
{
	if (info->conversion == 'b')
		return (binary_get_string(ap, info));
	else if (info->conversion == 'o' || info->conversion == 'O')
		return (octal_get_string(ap, info));
	else
		return (hexa_get_string(ap, info));
}
