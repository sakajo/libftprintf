/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wide_print_argument.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:21:29 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/18 19:26:11 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	wide_print_argument(va_list ap, t_info *info, int *bytes)
{
	wchar_t	*wide_string;
	int		zero_character;

	zero_character = 0;
	wide_string = wide_get_wide_string(ap, info->conversion, &zero_character);
	wide_string_edit_precision(&wide_string,
			info->precision, info->conversion);
	wide_string_edit_width(&wide_string, info, zero_character);
	wide_string_print(wide_string, zero_character, bytes);
}
