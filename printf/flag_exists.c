/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_exists.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 20:56:15 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:24:23 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		flag_exists(const char *try, char *argument)
{
	int		index_arg;
	int		index_try;

	index_arg = 0;
	while (argument[index_arg] != '\0')
	{
		index_try = 0;
		while (try[index_try] != '\0' &&
				try[index_try] == argument[index_arg + index_try])
			index_try++;
		if (try[index_try] == '\0')
		{
			if (!ft_strequ("0", try) || index_arg == 0)
				return (1);
			else if (!ft_isdigit(argument[index_arg - 1]) &&
					argument[index_arg - 1] != '.')
				return (1);
		}
		index_arg++;
	}
	return (0);
}
