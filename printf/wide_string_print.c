/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wide_string_print.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 21:22:54 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/16 21:22:55 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	wide_string_print(wchar_t *wide_string, int zero_boolean, int *bytes)
{
	int		index;

	index = 0;
	while (wide_string[index] != '\0')
	{
		ft_putwchar(wide_string[index]);
		(*bytes) += ft_wide_char_size(wide_string[index]);
		index++;
	}
	if (zero_boolean)
	{
		ft_putwchar(wide_string[index]);
		(*bytes) += ft_wide_char_size(wide_string[index]);
		index++;
		while (wide_string[index] != '\0')
		{
			ft_putwchar(wide_string[index]);
			(*bytes) += ft_wide_char_size(wide_string[index]);
			index++;
		}
	}
}
